<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Servlet Job Finder</title>
		<style>
			div {
				margin-top: 5px;
				margin-bottom: 5px;
			}
			
			#container{
				display: flex;
				flex-direction: column;
				justify-content: center;
				align-items: center;
			}
			
			div > input, div > select, div > textarea{
				width: 60%;
			}
		</style>
	</head>
	<body>
		<div id="container">
		
			<h1>Welcome to Servlet Job Finder</h1>
			
			<form action="register" method="post">
		
				<div>
					<label for="fname">First Name</label>
					<input type="text" name="fname" required>
				</div>
				<div>
					<label for="lname">Last Name</label>
					<input type="text" name="lname" required>
				</div>
				<div>
					<label for="phone">Phone</label>
					<input type="tel" name="phone" required>
				</div>
				<div>
					<label for="email">Email</label>
					<input type="email" name="email" required>
				</div>
			
				<!--  Vehicle Choice -->
				<fieldset>
					<legend>How did you discover the app?</legend>
					<input type="radio" id="friends" name="dis_app" value="friends" required>
					<label for="friends">Friends</label>
					<br>
					<input type="radio" id="soc_med" name="dis_app" value="soc_med" required>
					<label for="soc_med">Social Media</label>
					<br>
					<input type="radio" id="others" name="dis_app" value="others" required>
					<label for="others">Others</label>
				</fieldset>			
				<div>
					<label for ="birth_date">Date of Birth</label>
					<input type="date" name="birth_date" required>
				</div>
			
				<div>
					<label for ="user_type">Are you an employer or an applicant?</label>
					<select id="user" name="user_type">
						<option value="" selected="selected">Select One</option>
						<option value="employeer">Employer</option>
						<option value="applicant">Applicant</option>
					</select>
				</div>			
				<!-- Special Instructions -->
				<div>
					<label for="description">Profile Description</label>
					<textarea name="description" maxlength="500"></textarea>
				</div>
			
				<button>Register</button>
			
			</form>
		</div>
	</body>
</html>