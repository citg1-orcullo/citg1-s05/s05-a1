package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3834552629113201265L;
	
	public void init() throws ServletException{
		System.out.println("RegisterServlet has been initialized.");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String fname = req.getParameter("fname");
		String lname = req.getParameter("lname"); 
		String phone = req.getParameter("phone");                
		String email = req.getParameter("email");                
		String disApp = req.getParameter("dis_app");                             
		String birthDate = req.getParameter("birth_date");                              
		String userType = req.getParameter("user_type");                
		String description = req.getParameter("description");
		
		//store all the data from the form into the session
		HttpSession session = req.getSession();
		
		session.setAttribute("fname", fname);
		session.setAttribute("lname", lname);
		session.setAttribute("phone", phone);
		session.setAttribute("email", email);
		session.setAttribute("dis_app", disApp);
		session.setAttribute("birth_date", birthDate);
		session.setAttribute("user_type", userType);
		session.setAttribute("description", description);
		
		res.sendRedirect("register.jsp"); //to redirect, checking the information
	}
	
	public void destroy() {
		System.out.println("RegisterServlet has been finalized");
	}

}
